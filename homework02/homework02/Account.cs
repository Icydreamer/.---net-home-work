﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework02
{
    class Account
    {
        private string? accountNum;
        private string? password;
        private double? deposit;
        public string? AccountNum 
        {
            get { return accountNum; }
            set { accountNum = value; }
        }
        public string? Password
        {
            get { return password; }
            set { password = value; }
        }
        public double? Deposit
        {
            get { return deposit; }
            set { deposit = value; }
        }

        public Account()
        {
            AccountNum = 0.ToString();
            Deposit = 0;
        }
        public Account(string account, string password)
        {
            AccountNum = account;
            Password = password;
            Deposit = 0;
        }
        public Account(string account, string password, double money)
        {
            AccountNum = account;
            Password = password;
            Deposit = money;
        }

        public bool Withdraw(double money)
        {
            if (money >= Deposit)
            {
                return false;
            }
            else
            {
                Deposit -= money;
                return true;
            }
        }
        public bool Save(double money)
        {
            if (money < 0) return false;
            Deposit += money;
            return true;
        }
        public bool IsMatch(string account, string password)
        {
            return account == AccountNum && password == Password;
        }
    }
}
