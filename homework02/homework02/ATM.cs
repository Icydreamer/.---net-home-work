﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework02
{
    class ATM
    {
        public event BigMoneyFetchedHandler OnBigMoneyFetched;
        public void BigMoneyFetched(double deposit)
        {
            BigMoneyArgs args = new BigMoneyArgs()
            {
                Deposit = deposit
            };
            OnBigMoneyFetched(this, args);
        }
        private Bank bank;
        public ATM(Bank bank)
        {
            this.bank = bank;
        }
        public void Transaction()
        {
            Console.WriteLine("please insert your card");
            string? id = Console.ReadLine();
            Console.WriteLine("please enter your password");
            string? pwd = Console.ReadLine();
            Account account = bank.FindAccount(id, pwd);
            if (account == null)
            {
                Console.WriteLine("card invalid or password not corrent");
                return;
            }
            Console.WriteLine("1: display; 2: save; 3: withdraw");
            string? op = Console.ReadLine();
            if (op == "1")
            {
                Console.WriteLine("balance: " + account.Deposit);
            }
            else if (op == "2")
            {
                Console.WriteLine("save money");
                string? smoney = Console.ReadLine();
                double money = double.Parse(smoney);
                bool ok = account.Save(money);
                if (ok) Console.WriteLine("ok");
                else Console.WriteLine("eeer");
                Console.WriteLine("balance: " + account.Deposit);
            }
            else if (op == "3")
            {
                Console.WriteLine("withdraw money");
                string? smoney = Console.ReadLine();
                double money = double.Parse(smoney);
                bool ok = account.Withdraw(money);
                if (ok) Console.WriteLine("ok");
                else Console.WriteLine("eeer");
                Console.WriteLine("balance: " + account.Deposit);
            }
        }
    }
}
public delegate void BigMoneyFetchedHandler(object sender, BigMoneyArgs args);
public class BigMoneyArgs
{
    public double Deposit {  get; set; }
}
