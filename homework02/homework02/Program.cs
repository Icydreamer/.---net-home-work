﻿// See https://aka.ms/new-console-template for more information
using homework02;
Bank bank = new Bank();
ATM atm = new ATM(bank);
Account account = new Account("123", "123", 100);
atm.OnBigMoneyFetched += new BigMoneyFetchedHandler(Atm_OnBigMoneyFetched);

while (true)
{
    Console.Write("插入银行卡: ");
    string no = Console.ReadLine();
    if (account.AccountNum == no)
    {
        Console.Write("请输入密码: ");
        string password = Console.ReadLine();
        if (account.Password == password)
        {
            bool temp = true;
            do
            {
                Console.WriteLine("1. 查询余额");
                Console.WriteLine("2. 存款");
                Console.WriteLine("3. 取款");
                Console.WriteLine("4. 修改密码");
                Console.WriteLine("5. 转账");
                Console.WriteLine("6. 退卡");
                Console.Write("请选择: ");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("您的余额为: " + account.Deposit);
                        break;
                    case 2:
                        Console.Write("请放入钞票: ");
                        int money1 = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            if (account.Save(money1))
                            {
                                Console.WriteLine("存入成功!!!");
                            }
                            else
                            {
                                Console.WriteLine("无法识别!!!");
                            }
                        }
                        catch (BadCashException e)
                        {
                            Console.WriteLine("错误代码为：" + e.Code);
                        }
                        
                        break;
                    case 3:
                        Console.Write("请输入取款金额: ");
                        int money2 = Convert.ToInt32(Console.ReadLine());
                        if (account.Withdraw(money2))
                        {
                            Console.WriteLine("请从出钞口取走钞票");
                        }
                        else
                        {
                            Console.WriteLine("余额不足!!!");
                        }
                        break;
/*                    case 4:
                        Console.Write("请输入原密码: ");
                        string oldPass = Console.ReadLine();
                        Console.Write("请输入新密码: ");
                        string newPass1 = Console.ReadLine();
                        Console.Write("请再次输入新密码: ");
                        string newPass2 = Console.ReadLine();
                        if (newPass1 == newPass2)
                        {
                            if (atm.ChangePassword(oldPass, newPass1))
                            {
                                Console.WriteLine("修改成功!!!");
                            }
                            else
                            {
                                Console.WriteLine("修改失败!!!");
                            }
                        }
                        else
                        {
                            Console.WriteLine("两次输入不一致!!!");
                        }
                        break;*/
/*                    case 5:
                        Console.Write("请输入转入账户的账号: ");
                        string toAccountNo = Console.ReadLine();
                        Console.Write("请输入转账金额: ");
                        double money3 = Convert.ToDouble(Console.ReadLine());
                        if (atm.Transfer(toAccountNo, money3))
                        {
                            Console.WriteLine("转账成功!!!");
                        }
                        else
                        {
                            Console.WriteLine("转账失败!!!");
                        }
                        break;*/
                    case 6:
                        temp = false;
                        break;
                }
            } while (temp);
        }
        else
        {
            Console.WriteLine("密码错误!!!");
        }
    }
}
void Atm_OnBigMoneyFetched(object sender, BigMoneyArgs args)
{
    Console.WriteLine("金额过大");
}