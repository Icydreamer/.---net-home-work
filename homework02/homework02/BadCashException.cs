﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework02
{
    public class BadCashException : Exception
    {
        private int code;
        public BadCashException(String message, int code) : base(message)
        {
            this.code = code;
        }
        public int Code { get { return code; } }
        public static void BadCash(string name, int num)
        {
            throw new BadCashException("坏钞票", 3);
        }
    }
}
