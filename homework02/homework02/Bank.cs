﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework02
{
    class Bank
    {
        List<Account> accounts = new List<Account>();
        public Account OpenAccount(string accountNum, string password, double money)
        {
            Account account = new Account(accountNum, password, money);
            accounts.Add(account);
            return account;
        }
        public bool CloseAccount(Account account)
        {
            int index = accounts.IndexOf(account);
            if (index < 0) return false;
            accounts.Remove(account);
            return true;
        }
        public Account FindAccount(string accountNum, string password)
        {
            foreach (Account account in accounts)
            {
                if (account.IsMatch(accountNum, password))
                {
                    return account;
                }
            }
            return null;
        }
    }
}
