﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework02
{
    class CreditAccount : Account
    {
        private double? creditLine;
        public double? CreditLine
        {
            get { return creditLine; }
            set { creditLine = value; }
        }
        public bool GetCredit(double money)
        {
            if (money > CreditLine)
            {
                return false;
            }
            else
            {
                CreditLine -= money;
                Deposit += money;
                return true;
            }
        }
    }
}
