﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CrawlerBot
{
    class urlstates
    {
        public string url { get; set; }
        public bool processing { get; set; }
        public string html { get; set; }

    }
    class Crawler
    {
        public event Action<Crawler, urlstates> PageDownloaded;
        //使用线程安全的集合
        public ConcurrentBag<urlstates> urls = new ConcurrentBag<urlstates>();
        public int count = 0;
        static public string startUrl = "";
        static public string startWith = "";

        public void Crawl()
        {
            urlstates surl = new urlstates() { url = startUrl, processing = false, html = "" };
            urls.Add(surl);
            string str = @"(www\.){0,1}.*?\..*?/";
            Regex r = new Regex(str);
            Match m = r.Match(startWith);
            startWith = m.Value;

            while (true)
            {
                urlstates current = null;
                foreach (var url in urls)
                {
                    if (url.processing) continue;
                    current = url;
                    if (count > 20) { break; }
                    if (current == null) { continue; }
                    current.processing = true;
                    var t = new Thread(() => Process(current));
                    t.Start();
                    count++;
                }
            }
        }

        private void Process(urlstates current)
        {
            throw new NotImplementedException();
        }
    }
}
