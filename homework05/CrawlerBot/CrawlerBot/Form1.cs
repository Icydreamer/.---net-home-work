﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrawlerBot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private string Search(string url, int start, int end)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                WebResponse response = request.GetResponse();
                Stream resStream = response.GetResponseStream();
                StreamReader sr = new StreamReader(resStream, Encoding.UTF8);
                string webInfo = sr.ReadToEnd();
                string webData = "";
                //限定为汉字
                string valid = @"[\u4E00-\u9FFF]+";
                foreach (Match match in Regex.Matches(webInfo, valid))
                {
                    webData += match;
                }
                return webData.Substring(start, end);
            }
            catch (Exception e)
            {
                //如果异常返回空串
                return "查询出错！！！";
                MessageBox.Show(e.ToString());

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Task task_baidu = new Task(() => { richTextBox2.Text = Search("https://cn.bing.com/search?q=" + textBox1.Text, 50, 200); });
            Task task_bing = new Task(() => { richTextBox1.Text = Search("https://baike.baidu.com/item/" + textBox1.Text, 0, 200); });
            task_baidu.Start();
            task_bing.Start();
        }
    }
}
