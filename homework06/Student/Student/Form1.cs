namespace Student
{
    public partial class Form1 : Form
    {
        SchInfoContext db = new SchInfoContext();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add($"Datavase path: {db.DbPath}.");

            // Create stu464
            listBox1.Items.Add("Inserting a new student-464");
            Student s1 = new Student { Id = 464, Name = "LJH" };
            db.Add(s1);
            db.SaveChanges();

            //Add to logs
            db.logs.Add(new LogItem { Action = "Add Student", ItemId = s1.Id });
            db.SaveChanges();

            // Read
            listBox1.Items.Add("Querying for students");
            var stu = db.students
                .OrderBy(b => b.Id)
                .First();

            // Update
            listBox1.Items.Add("Updating the student and adding a class");
            stu.Id = 465;
            db.SaveChanges();

            //Add to logs
            db.logs.Add(new LogItem { Action = "Update Student", ItemId = stu.Id });
            db.SaveChanges();

            //Add a class
            // Create new class and add student-464 to it
            listBox1.Items.Add("Inserting a new class with student-465");
            var newClass = new Class { Name = "Class-101", SchoolId = 1 };
            newClass.Students = new List<Student> { s1 };
            db.Add(newClass);
            db.SaveChanges();

            //Add to logs
            db.logs.Add(new LogItem { Action = "Add Class", ItemId = 101 });
            db.SaveChanges();


            // Delete
            listBox1.Items.Add("Delete the db");
            db.Remove(stu);
            db.SaveChanges();

            //Add to logs
            db.logs.Add(new LogItem { Action = "Delete db", ItemId = 0 });
            db.SaveChanges();
        }
    }
}