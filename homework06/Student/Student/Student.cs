﻿using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
namespace Student
{
    public class SchInfoContext : DbContext
    {
        public DbSet<School> schools { get; set; }
        public DbSet<Class> classes { get; set; }
        public DbSet<Student> students { get; set; }
        public DbSet<LogItem> logs { get; set; }
        public string DbPath { get; }
        public SchInfoContext()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "SchInfo.db");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Class> Classes { get; set; }
    }

    public class Class
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolId { get; set; }
        public List<Student> Students { get; set; }
    }

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassId { get; set; }
    }

    public class LogItem
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public int ItemId { get; set; }
    }
}