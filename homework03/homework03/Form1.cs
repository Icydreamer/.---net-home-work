using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Diagnostics;
using GroupDocs.Parser;
namespace homework03
{
    public partial class Form1 : Form
    {
        string openedFilePathVar;
        string newFilePath;
        int lineNum = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                openedFilePathVar = openFileDialog1.FileName;
                fileName.Text = Path.GetFileName(openedFilePathVar);
                filePath.Text = Path.GetFullPath(openedFilePathVar);
            }
            try
            {
                newFilePath = Path.Combine(Environment.CurrentDirectory, "Test.txt");
                FileStream fin = new FileStream(openedFilePathVar, FileMode.Open, FileAccess.Read);
                FileStream fout = new FileStream(newFilePath, FileMode.Create, FileAccess.Write);
                using (var reader = new StreamReader(fin))
                {
                    using (var writer = new StreamWriter(fout))
                    {
                        int nullLineCount = 0;
                        int noteLineCount = 0;
                        string? s = null;
                        while ((s = reader.ReadLine()) != null)
                        {
                            if (s == "" || s.Length < 1)
                            {
                                nullLineCount++;
                            }
                            else if (s.IndexOf("//") != -1)
                            {
                                noteLineCount++;
                            }
                            else
                            {
                                lineNum++;
                                writer.WriteLine(s);
                            }
                        }
                        lineNumLabel.Text = "原始行数为：" + lineNum.ToString();
                        writer.Flush();
                        writer.Close();
                    }
                }
                // Process.Start("notepad.exe", newFilePath);
                using (var reader = new StreamReader(newFilePath))
                {
                    using (var parser = new Parser(newFilePath))
                    {
                        Dictionary<string, int> stats = new Dictionary<string, int>();
                        string text = reader.ReadToEnd();
                        char[] chars = { ' ', '.', ',', ';', ':', '?', '(', ')', '\n', '\r' };
                        string[] words = text.Split(chars);
                        int minWordLength = 2;
                        foreach (string word in words)
                        {
                            string w = word.Trim().ToLower();
                            if (w.Length > minWordLength)
                            {
                                if (!stats.ContainsKey(w))
                                {
                                    stats.Add(w, 1);
                                }
                                else
                                {
                                    stats[w]++;
                                }
                            }
                        }
                        wordNumLabel.Text = "原始单词数为：" + stats.Count.ToString();
                        var orderStats = stats.OrderByDescending(x => x.Value);
                        foreach (var w in orderStats)
                        {
                            var key = w.Key;
                            var value = w.Value;
                            string item = "Total occurrences of " + key + ": " + value;
                            listBox1.Items.Add(item);
                        }
                    }
                }
            }
            catch (FileNotFoundException e1)
            {
                Console.WriteLine(e1.Message);
            }
            catch (IOException e2)
            {
                Console.Write(e2.Message);
            }
        }
    }
}