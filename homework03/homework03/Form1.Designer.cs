﻿namespace homework03
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            openFileDialog1 = new OpenFileDialog();
            filePathLabel = new Label();
            fileNameLabel = new Label();
            fileName = new Label();
            filePath = new Label();
            openFileButton = new Button();
            listBox1 = new ListBox();
            lineNumLabel = new Label();
            wordNumLabel = new Label();
            SuspendLayout();
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // filePathLabel
            // 
            filePathLabel.AutoSize = true;
            filePathLabel.Location = new Point(145, 265);
            filePathLabel.Margin = new Padding(6, 0, 6, 0);
            filePathLabel.Name = "filePathLabel";
            filePathLabel.Size = new Size(117, 28);
            filePathLabel.TabIndex = 0;
            filePathLabel.Text = "文件路径：";
            // 
            // fileNameLabel
            // 
            fileNameLabel.AutoSize = true;
            fileNameLabel.Location = new Point(145, 186);
            fileNameLabel.Margin = new Padding(6, 0, 6, 0);
            fileNameLabel.Name = "fileNameLabel";
            fileNameLabel.Size = new Size(96, 28);
            fileNameLabel.TabIndex = 1;
            fileNameLabel.Text = "文件名：";
            // 
            // fileName
            // 
            fileName.AutoSize = true;
            fileName.Location = new Point(310, 186);
            fileName.Margin = new Padding(6, 0, 6, 0);
            fileName.Name = "fileName";
            fileName.Size = new Size(73, 28);
            fileName.TabIndex = 2;
            fileName.Text = "label1";
            // 
            // filePath
            // 
            filePath.AutoSize = true;
            filePath.Location = new Point(310, 265);
            filePath.Margin = new Padding(6, 0, 6, 0);
            filePath.Name = "filePath";
            filePath.Size = new Size(73, 28);
            filePath.TabIndex = 3;
            filePath.Text = "label1";
            // 
            // openFileButton
            // 
            openFileButton.Location = new Point(145, 334);
            openFileButton.Margin = new Padding(6, 5, 6, 5);
            openFileButton.Name = "openFileButton";
            openFileButton.Size = new Size(139, 38);
            openFileButton.TabIndex = 4;
            openFileButton.Text = "打开文件";
            openFileButton.UseVisualStyleBackColor = true;
            openFileButton.Click += openFileButton_Click;
            // 
            // listBox1
            // 
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 28;
            listBox1.Location = new Point(780, 68);
            listBox1.Margin = new Padding(6, 5, 6, 5);
            listBox1.Name = "listBox1";
            listBox1.Size = new Size(602, 536);
            listBox1.TabIndex = 5;
            // 
            // lineNumLabel
            // 
            lineNumLabel.AutoSize = true;
            lineNumLabel.Location = new Point(158, 436);
            lineNumLabel.Name = "lineNumLabel";
            lineNumLabel.Size = new Size(138, 28);
            lineNumLabel.TabIndex = 6;
            lineNumLabel.Text = "原始行数为：";
            // 
            // wordNumLabel
            // 
            wordNumLabel.AutoSize = true;
            wordNumLabel.Location = new Point(158, 497);
            wordNumLabel.Name = "wordNumLabel";
            wordNumLabel.Size = new Size(159, 28);
            wordNumLabel.TabIndex = 7;
            wordNumLabel.Text = "原始单词数为：";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(13F, 28F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1486, 741);
            Controls.Add(wordNumLabel);
            Controls.Add(lineNumLabel);
            Controls.Add(listBox1);
            Controls.Add(openFileButton);
            Controls.Add(filePath);
            Controls.Add(fileName);
            Controls.Add(fileNameLabel);
            Controls.Add(filePathLabel);
            Margin = new Padding(6, 5, 6, 5);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private OpenFileDialog openFileDialog1;
        private Label filePathLabel;
        private Label fileNameLabel;
        private Label fileName;
        private Label filePath;
        private Button openFileButton;
        private ListBox listBox1;
        private Label lineNumLabel;
        private Label wordNumLabel;
    }
}