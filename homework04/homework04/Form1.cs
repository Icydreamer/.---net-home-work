﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FileBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void 打开文件夹ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {

                var rootNode = treeView1.Nodes.Add(folderBrowserDialog1.SelectedPath);
                rootNode.Tag = folderBrowserDialog1.SelectedPath;

                AddDirectoriesAndFiles(folderBrowserDialog1.SelectedPath, rootNode);
            }
        }
        private void AddDirectoriesAndFiles(string path, TreeNode parentNode)
        {
            try
            {
                foreach (var directoryPath in Directory.GetDirectories(path))
                {
                    var directoryInfo = new DirectoryInfo(directoryPath);
                    var directoryNode = parentNode.Nodes.Add(directoryInfo.Name);
                    directoryNode.Tag = directoryPath;

                    // 添加子目录和文件
                    AddDirectoriesAndFiles(directoryPath, directoryNode);
                }

                // 添加当前目录下的文件
                foreach (var filePath in Directory.GetFiles(path))
                {
                    var fileInfo = new FileInfo(filePath);
                    var fileNode = parentNode.Nodes.Add(fileInfo.Name);
                    fileNode.Tag = filePath;
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
                // 没有足够权限访问目录
            }
        }
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null && File.Exists((string)e.Node.Tag))
            {
                // 获取文件路径
                string filePath = this.treeView1.SelectedNode.FullPath;
                if (Path.GetExtension(filePath).Equals(".txt", StringComparison.OrdinalIgnoreCase))
                {
                    Process.Start("notepad.exe", filePath);
                }
                if (Path.GetExtension(filePath).Equals(".exe",StringComparison.OrdinalIgnoreCase))
                {
                    Process.Start(filePath);
                }
            }
        }
        void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var a = e.Node.FullPath;
            var dialog = new FolderBrowserDialog();
            dialog.SelectedPath = a;
            PopulateFileList(dialog.SelectedPath);
        }
        private void PopulateFileList(string directoryPath)
        {
            listBox1.Items.Clear();

            var directoryInfo = new DirectoryInfo(directoryPath);

            // 显示文件夹
            foreach (var subDirectoryInfo in directoryInfo.GetDirectories())
            {
                var item = new ListViewItem(subDirectoryInfo.Name);
                item.SubItems.Add("");
                item.SubItems.Add(subDirectoryInfo.LastWriteTime.ToString());
                item.Tag = subDirectoryInfo.FullName;
                listBox1.Items.Add(item);
            }
            // 显示文件
            foreach (var fileInfo in directoryInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly))
            {
                var item = new ListViewItem(fileInfo.Name);
                item.SubItems.Add(fileInfo.Length.ToString());
                item.SubItems.Add(fileInfo.LastWriteTime.ToString());
                item.Tag = fileInfo.FullName;
                listBox1.Items.Add(item);
            }
        }
    }
}
