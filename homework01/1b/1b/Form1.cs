namespace _1b
{
    public partial class Form1 : Form
    {
        int first;
        int second;
        int score = 0; // 分数
        int count = 10; // 出题总数
        int n = 0;
        public Form1()
        {
            InitializeComponent();
        }
        // 绑定回车
        private void ResultNumberBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)//如果输入的是回车键  
            {
                if (n < count)
                {
                    n++;
                    if (checkResult())
                    {
                        checkedTrue();
                        setQuestion();
                    }
                    else
                    {
                        checkedFalse();
                        setQuestion();
                    }
                }
                else
                {
                    messageLabel.Text = "游戏结束";
                    timer1.Stop();
                }
            }
        }
        // 开始
        private void StartButton_Click(object sender, EventArgs e)
        {
            timer1.Start();
            setQuestion();
        }
        // 出题
        private void setQuestion()
        {
            //设置随机数
            Random rd1 = new Random();

            //生成1-10的随机数
            firstNumberLabel.Text = rd1.Next(1, 10).ToString();
            secondNumberLabel.Text = rd1.Next(1, 10).ToString();

            first = int.Parse(firstNumberLabel.Text);
            second = int.Parse(secondNumberLabel.Text);
            //随机加减法
            if (rd1.Next(1, 10) > 5)
            {
                opLabel.Text = "+";
            }
            else
            {
                opLabel.Text = "-";
                second *= -1;
            }

        }
        // 检查是否正确
        private bool checkResult()
        {
            int result = 0;
            try
            {
                result = int.Parse(resultNumberBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("输入错误！");
            }
            if (result == first + second)
            {
                return true;
            }
            return false;
        }
        private void checkedTrue()
        {
            score++;
            scoreNumberLabel.Text = score.ToString();
            resultNumberBox.Text = null;
            messageLabel.Text = "回答正确！";
        }
        private void checkedFalse()
        {
            resultNumberBox.Text = null;
            messageLabel.Text = "回答错误！";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int second = 0;
            second++;
            timeLabel.Text = second.ToString();
        }
    }
}