﻿namespace _1b
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            firstNumberLabel = new Label();
            secondNumberLabel = new Label();
            opLabel = new Label();
            scoreNumberLabel = new Label();
            equalLabel = new Label();
            resultNumberBox = new TextBox();
            scoreLabel = new Label();
            timeLabel = new Label();
            timeUsedLabel = new Label();
            startButton = new Button();
            messageLabel = new Label();
            timer1 = new System.Windows.Forms.Timer(components);
            SuspendLayout();
            // 
            // firstNumberLabel
            // 
            firstNumberLabel.AutoSize = true;
            firstNumberLabel.Location = new Point(171, 301);
            firstNumberLabel.Margin = new Padding(6, 0, 6, 0);
            firstNumberLabel.Name = "firstNumberLabel";
            firstNumberLabel.Size = new Size(134, 28);
            firstNumberLabel.TabIndex = 1;
            firstNumberLabel.Text = "firstNumber";
            // 
            // secondNumberLabel
            // 
            secondNumberLabel.AutoSize = true;
            secondNumberLabel.Location = new Point(630, 301);
            secondNumberLabel.Margin = new Padding(6, 0, 6, 0);
            secondNumberLabel.Name = "secondNumberLabel";
            secondNumberLabel.Size = new Size(167, 28);
            secondNumberLabel.TabIndex = 2;
            secondNumberLabel.Text = "secondNumber";
            // 
            // opLabel
            // 
            opLabel.AutoSize = true;
            opLabel.Location = new Point(461, 301);
            opLabel.Margin = new Padding(6, 0, 6, 0);
            opLabel.Name = "opLabel";
            opLabel.Size = new Size(38, 28);
            opLabel.TabIndex = 3;
            opLabel.Text = "op";
            // 
            // scoreNumberLabel
            // 
            scoreNumberLabel.AutoSize = true;
            scoreNumberLabel.Location = new Point(1206, 78);
            scoreNumberLabel.Margin = new Padding(6, 0, 6, 0);
            scoreNumberLabel.Name = "scoreNumberLabel";
            scoreNumberLabel.Size = new Size(24, 28);
            scoreNumberLabel.TabIndex = 7;
            scoreNumberLabel.Text = "0";
            // 
            // equalLabel
            // 
            equalLabel.AutoSize = true;
            equalLabel.Location = new Point(865, 306);
            equalLabel.Margin = new Padding(6, 0, 6, 0);
            equalLabel.Name = "equalLabel";
            equalLabel.Size = new Size(28, 28);
            equalLabel.TabIndex = 8;
            equalLabel.Text = "=";
            // 
            // resultNumberBox
            // 
            resultNumberBox.Location = new Point(992, 308);
            resultNumberBox.Margin = new Padding(6, 5, 6, 5);
            resultNumberBox.Name = "resultNumberBox";
            resultNumberBox.Size = new Size(182, 34);
            resultNumberBox.TabIndex = 10;
            resultNumberBox.KeyDown += ResultNumberBox_KeyDown;
            // 
            // scoreLabel
            // 
            scoreLabel.AutoSize = true;
            scoreLabel.Location = new Point(1111, 78);
            scoreLabel.Name = "scoreLabel";
            scoreLabel.Size = new Size(75, 28);
            scoreLabel.TabIndex = 12;
            scoreLabel.Text = "分数：";
            // 
            // timeLabel
            // 
            timeLabel.AutoSize = true;
            timeLabel.Location = new Point(1206, 136);
            timeLabel.Name = "timeLabel";
            timeLabel.Size = new Size(24, 28);
            timeLabel.TabIndex = 13;
            timeLabel.Text = "0";
            // 
            // timeUsedLabel
            // 
            timeUsedLabel.AutoSize = true;
            timeUsedLabel.Location = new Point(1111, 136);
            timeUsedLabel.Name = "timeUsedLabel";
            timeUsedLabel.Size = new Size(75, 28);
            timeUsedLabel.TabIndex = 14;
            timeUsedLabel.Text = "用时：";
            // 
            // startButton
            // 
            startButton.Location = new Point(978, 415);
            startButton.Name = "startButton";
            startButton.Size = new Size(131, 40);
            startButton.TabIndex = 17;
            startButton.Text = "开始";
            startButton.UseVisualStyleBackColor = true;
            startButton.Click += StartButton_Click;
            // 
            // messageLabel
            // 
            messageLabel.AutoSize = true;
            messageLabel.Location = new Point(412, 415);
            messageLabel.Name = "messageLabel";
            messageLabel.Size = new Size(102, 28);
            messageLabel.TabIndex = 18;
            messageLabel.Text = " 提示信息";
            // 
            // timer1
            // 
            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(13F, 28F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1486, 741);
            Controls.Add(messageLabel);
            Controls.Add(startButton);
            Controls.Add(timeUsedLabel);
            Controls.Add(timeLabel);
            Controls.Add(scoreLabel);
            Controls.Add(resultNumberBox);
            Controls.Add(equalLabel);
            Controls.Add(scoreNumberLabel);
            Controls.Add(opLabel);
            Controls.Add(secondNumberLabel);
            Controls.Add(firstNumberLabel);
            Margin = new Padding(6, 5, 6, 5);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label firstNumberLabel;
        private Label secondNumberLabel;
        private Label opLabel;
        private Label scoreNumberLabel;
        private Label equalLabel;
        private TextBox resultNumberBox;
        private Label scoreLabel;
        private Label timeLabel;
        private Label timeUsedLabel;
        private Button startButton;
        private Label messageLabel;
        private System.Windows.Forms.Timer timer1;
    }
}